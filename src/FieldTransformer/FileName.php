<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\ContentImport\Service\Messages;
use ContextualCode\Crawler\Entity\Page;
use ContextualCode\Crawler\Repository\PageRepository;
use DOMElement;

class FileName extends Base
{
    /** @var PageRepository */
    protected $pageRepository;

    public function __construct(Messages $messages, PageRepository $pageRepository)
    {
        parent::__construct($messages);

        $this->pageRepository = $pageRepository;
    }

    public function getServiceIdentifier(): string
    {
        return 'file-name';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $name = $this->getFileName($page, ['title', 'href'], true);

        $value = new ContentFieldValue($fieldName, $name);
        $this->checkValueIsRequired($params, $value);

        return $value;
    }

    public function getFileName(
        Page $page,
        array $nameAttributes = [],
        bool $useNodeValue = false
    ): ?string {
        $name = null;
        $context = [
            'url' => $page->getUrl(),
            'referer_url' => $page->getReferer(),
            'referer_path' => $page->getRefererPath(),
        ];

        $originalUrl = $page->getReferer();

        $forcedHttpsUrl = str_replace('http://', 'https://', $originalUrl);

        $possibleUrls = [
            $originalUrl,
            $forcedHttpsUrl,
            str_replace('/index.shtml', '/', $originalUrl),
            str_replace('/index.shtml', '/', $forcedHttpsUrl),
        ];

        foreach ($possibleUrls as $possibleUrl) {
            $referer = $this->pageRepository->findOneBy([
                'identifier' => $page->getIdentifier(),
                'url' => $possibleUrl,
            ]);
            if ($referer) {
                break;
            }
        }
        if (null === $referer) {
            $this->invalidContentField('error_no_referer', [$page->getUrl()], $context);
        }

        $elements = $referer->getResponseXPath()->query($page->getRefererPath());
        if ($elements->length === 0) {
            $this->invalidContentField('error_no_dom_element', [$page->getRefererPath()], $context);
        }

        $element = $elements->item(0);
        if (!$element instanceof DOMElement) {
            $this->invalidContentField('error_invalid_dom_element', [], $context);
        }

        foreach ($nameAttributes as $nameAttribute) {
            if ($element->hasAttribute($nameAttribute)) {
                $name = (string) $element->getAttribute($nameAttribute);
            }

            if (!empty($name)) {
                if ($nameAttribute === 'href') {
                    // just use the basename of the URL
                    $name = basename($name);
                }
                break;
            }
        }

        if (empty($name) && $useNodeValue) {
            $name = trim((string) $element->nodeValue);
        }

        if (empty($name)) {
            $name = strtok(basename($page->getUrl()), '?');
        }

        return $name;
    }
}
