<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

class StaticValue extends Base
{
    public const PARAM_VALUE = 'value';

    public function getServiceIdentifier(): string
    {
        return 'static-value';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $value = $this->getRequiredParameter($params, self::PARAM_VALUE);

        return new ContentFieldValue($fieldName, $value);
    }
}
