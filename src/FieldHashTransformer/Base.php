<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldHashTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

abstract class Base
{
    abstract public function getServiceIdentifier(): string;

    abstract public function getAttributes(
        Page $page,
        ContentFieldValueInterface $fieldValue,
        array $params = []
    ): array;
}
