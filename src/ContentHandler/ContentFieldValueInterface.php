<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

interface ContentFieldValueInterface
{
    public function setName(string $name): void;

    public function setValue($value): void;

    public function getName(): string;

    public function getValue();
}
