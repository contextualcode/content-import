<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

use ContextualCode\ContentImport\FieldHashTransformer\Value as ValueFieldHashTransformer;
use ContextualCode\Crawler\Entity\Page;

abstract class ContentHandler
{
    abstract public function getImportIdentifier(): string;

    abstract public function doSupport(Page $page): bool;

    abstract public function getContentType(PageContentItem $contentItem): ContentTypeInterface;

    abstract public function getParentLocation(Page $page, ?string $DOMSelector): LocationInterface;

    abstract public function getFieldTransformers(Page $page, PageContentItem $contentItem): array;

    public function isActive(): bool
    {
        return true;
    }

    public function isFinal(): bool
    {
        return true;
    }

    public function uniqueContent(): bool
    {
        return false;
    }

    public function getPriority(): int
    {
        return 0;
    }

    public function extractContentItems(Page $page): array
    {
        return [new PageContentItem($page, '', $page->getUrl())];
    }

    public function getHashAttributes(Page $page, PageContentItem $contentItem): array
    {
        $hashAttributes = [];
        $fieldTransformers = $this->getFieldTransformers($page, $contentItem);
        $fieldIdentifiers = array_keys($fieldTransformers);

        foreach ($fieldIdentifiers as $fieldIdentifier) {
            $hashAttributes[$fieldIdentifier] = ['id' => ValueFieldHashTransformer::SERVICE_IDENTIFIER];
        }

        return $hashAttributes;
    }

    public function getServiceIdentifier(): string
    {
        return $this->getImportIdentifier() . '-' . get_class($this);
    }
}
