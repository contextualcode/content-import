<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Service\Integration;

use ContextualCode\ContentImport\ContentHandler\LocationInterface;

class Location implements LocationInterface
{
    private $id;
    private $contentId;

    public function __construct(int $id, int $contentId)
    {
        $this->id = $id;
        $this->contentId = $contentId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContentId(): int
    {
        return $this->contentId;
    }
}
