<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

interface LocationInterface
{
    public function getId(): int;

    public function getContentId(): int;
}
