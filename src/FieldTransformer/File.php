<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\ContentImport\Service\Messages;
use ContextualCode\Crawler\Entity\Page;
use ContextualCode\Crawler\Helper\Link;
use ContextualCode\Crawler\Service\Handler as CrawlerHandler;

class File extends Base
{
    public const PARAM_URL = 'url';

    protected $fileDownloadsPath;

    /** @var FileName */
    protected $fileNameTransformer;

    public function __construct(
        Messages $messages,
        string $fileDownloadsPath,
        FileName $fileNameTransformer
    ) {
        parent::__construct($messages);

        $this->fileDownloadsPath = $fileDownloadsPath;
        $this->fileNameTransformer =$fileNameTransformer;
    }

    public function getServiceIdentifier(): string
    {
        return 'file';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        return new ContentFieldValue($fieldName, $this->getFileData($page, $params));
    }

    public function cleanup(ContentFieldValueInterface $fieldValue): void
    {
        $value = $fieldValue->getValue();

        if (file_exists($value['localUrl'])) {
            unlink($value['localUrl']);
        }
    }

    protected function getFileData(Page $page, array $params): array
    {
        $url = $this->getRequiredParameter($params, self::PARAM_URL);
        $file = $this->downloadFile($url);

        return [
            'originalUrl' => $url,
            'localUrl' => $file,
            'fileSize' => filesize($file),
            'fileName' => strtok(basename($url), '?'),
            'mimeType' => $page->getContentType(),
        ];
    }

    protected function downloadFile(string $fileUrl): string
    {
        $this->checkContainerDir();

        $localFile = tempnam($this->fileDownloadsPath, '');

        $curl = curl_init(Link::encodeUrl($fileUrl));
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FILE => fopen($localFile, 'wb+'),
            CURLOPT_TIMEOUT => 60,
            CURLOPT_USERAGENT => CrawlerHandler::USER_AGENT,
            CURLOPT_SSL_VERIFYPEER => false,
        ]);

        if (curl_exec($curl) === false) {
            $this->invalidContentField('error_unable_download_file', [$fileUrl, $localFile]);
        }

        return $localFile;
    }

    protected function checkContainerDir(): void
    {
        if (!file_exists($this->fileDownloadsPath) && !mkdir($this->fileDownloadsPath) && !is_dir($this->fileDownloadsPath)) {
            $this->invalidContentField('error_create_local_file', [$this->fileDownloadsPath]);
        }

        if (!is_writable($this->fileDownloadsPath)) {
            $this->invalidContentField('error_path_is_not_writable', [$this->fileDownloadsPath]);
        }
    }
}
