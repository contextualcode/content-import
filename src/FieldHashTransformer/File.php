<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldHashTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

class File extends Base
{
    public function getServiceIdentifier(): string
    {
        return 'file';
    }

    public function getAttributes(
        Page $page,
        ContentFieldValueInterface $fieldValue,
        array $params = []
    ): array {
        $fileData = $fieldValue->getValue();

        return [
            $fieldValue->getName() . '_original_url' => $fileData['originalUrl'],
            $fieldValue->getName() . '_size' => $fileData['fileSize'],
        ];
    }
}
