<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Exception;

use Exception;
use Throwable;

abstract class Base extends Exception
{
    /** @var array */
    protected $context;

    public function __construct($message = '', array $context = [], $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->context = $context;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function setContextKey(string $key, $value): void
    {
        $this->context[$key] = $value;
    }
}
