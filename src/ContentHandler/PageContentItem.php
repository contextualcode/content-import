<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

use ContextualCode\Crawler\Entity\Page;

class PageContentItem
{
    private string $hashUrl;
    private string $DOMSelector;
    private Page $page;

    public function __construct(Page $page, string $DOMSelector, ?string $hashUrl = null)
    {
        $this->setPage($page);
        $this->setDOMSelector($DOMSelector);

        if ($hashUrl === null) {
            $hashUrl = $page->getUrl() . '#' . md5($DOMSelector);
        }
        $this->setHashUrl($hashUrl);
    }

    public function setPage(Page $page): void
    {
        $this->page = $page;
    }

    public function getPage(): Page
    {
        return $this->page;
    }

    public function setHashUrl(string $hashUrl): void
    {
        $this->hashUrl = $hashUrl;
    }

    public function getHashUrl(): string
    {
        return $this->hashUrl;
    }

    public function setDOMSelector($DOMSelector): void
    {
        $this->DOMSelector = $DOMSelector;
    }

    public function getDOMSelector(): string
    {
        return $this->DOMSelector;
    }
}
