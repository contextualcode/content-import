<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\DependencyInjection;

use ContextualCode\ContentImport\ContentHandler\ContentHandler;
use ContextualCode\ContentImport\FieldHashTransformer\Base as FieldHashTransformer;
use ContextualCode\ContentImport\FieldTransformer\Base as FieldTransformer;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

class ContentImportExtension extends Extension implements PrependExtensionInterface
{
    public const TAG_CONTENT_HANDLER = 'contextualcode.contentimport.contenthandler';
    public const TAG_FIELD_TRANSFORMER = 'contextualcode.contentimport.fieldtransformer';
    public const TAG_FIELD_HASH_TRANSFORMER = 'contextualcode.contentimport.fieldhashtransformer';

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->registerForAutoconfiguration(ContentHandler::class)
            ->addTag(self::TAG_CONTENT_HANDLER);
        $container->registerForAutoconfiguration(FieldTransformer::class)
            ->addTag(self::TAG_FIELD_TRANSFORMER);
        $container->registerForAutoconfiguration(FieldHashTransformer::class)
            ->addTag(self::TAG_FIELD_HASH_TRANSFORMER);
    }

    public function prepend(ContainerBuilder $container): void
    {
        $this->prependExtension($container, 'monolog');
    }

    protected function prependExtension(
        ContainerBuilder $container,
        string $extension,
        ?string $configFileName = null
    ): void {
        if ($configFileName === null) {
            $configFileName = $extension;
        }

        $configFile = __DIR__ . '/../Resources/config/' . $configFileName . '.yaml';
        $config = Yaml::parseFile($configFile);
        $container->prependExtensionConfig($extension, $config);
        $container->addResource(new FileResource($configFile));
    }
}
