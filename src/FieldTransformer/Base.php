<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\ContentImport\Exception\InvalidContentField;
use ContextualCode\ContentImport\Service\Messages;
use ContextualCode\Crawler\Entity\Page;
use DOMElement;
use Exception;

abstract class Base
{
    public const PARAM_REQUIRED = 'required';

    /** @var Messages */
    protected $messages;

    public function __construct(Messages $messages)
    {
        $this->messages = $messages;
    }

    abstract public function getServiceIdentifier(): string;

    abstract public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface;

    public function cleanup(ContentFieldValueInterface $value): void
    {
    }

    protected function checkValueIsRequired(
        array $params,
        ContentFieldValueInterface $fieldValue
    ): void {
        $value = $fieldValue->getValue();

        if (null === $value && $this->isRequired($params)) {
            $this->invalidContentField('error_required_field_empty', [$fieldValue->getName()], ['value' => $value]);
        }
    }

    protected function isRequired(array $params): bool
    {
        return isset($params[self::PARAM_REQUIRED]);
    }

    protected function getRequiredParameter(array $params, string $param)
    {
        if (!isset($params[$param])) {
            $this->invalidContentField('error_required_param_missing', [$param]);
        }

        return $params[$param];
    }

    protected function selectElement(Page $page, string $selector): DOMElement
    {
        $context = ['selector' => $selector];

        try {
            $elements = $page->getResponseXPath()->query($selector);
        } catch (Exception $e) {
            $this->invalidContentField('error_response_to_dom_xpath', [], $context);
        }

        if ($elements === false || $elements->length === 0) {
            $this->invalidContentField('error_no_dom_element', [$selector], $context);
        }

        $element = $elements->item(0);
        if (!$element instanceof DOMElement) {
            $this->invalidContentField('error_no_dom_element', [$selector], $context);
        }

        return $element;
    }

    protected function invalidContentField(string $messageId, array $messageVars = [], array $context = []): void
    {
        $message = $this->messages->get($messageId, $messageVars);

        $context['field_transformer'] = self::class;
        if (0 === count($context)) {
            $context = $messageVars;
        }

        throw new InvalidContentField($message, $context);
    }
}
