<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

interface ContentInterface
{
    public function getId(): int;

    public function getName(): string;

    public function getVersion(): int;

    public function getMainLocation(): LocationInterface;

    public function getContentType(): ContentTypeInterface;
}
