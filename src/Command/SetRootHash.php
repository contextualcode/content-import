<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Command;

use ContextualCode\ContentImport\ContentHandler\ContentInterface as Content;
use ContextualCode\ContentImport\ContentHandler\ContentOperationsInterface;
use ContextualCode\ContentImport\ContentHandler\LocationInterface as Location;
use ContextualCode\ContentImport\Entity\Hash\Content as ContentHash;
use ContextualCode\ContentImport\Entity\Hash\Location as LocationHash;
use ContextualCode\ContentImport\Repository\Hash\ContentRepository as ContentHashRepository;
use ContextualCode\ContentImport\Repository\Hash\LocationRepository as LocationHashRepository;
use ContextualCode\ContentImport\Service\Messages;
use ContextualCode\Crawler\Service\Crawler;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetRootHash extends Command
{
    protected static $defaultName = 'content-import:set-root-hash';

    private const ARGUMENT_IDENTIFIER = 'identifier';
    private const ARGUMENT_LOCATION_ID = 'location_id';

    /** @var Messages */
    private $messages;

    /** @var Crawler */
    private $crawler;

    /** @var ContentHashRepository */
    private $contentHashRepository;

    /** @var LocationHashRepository */
    private $locationHashRepository;

    /** @var ContentOperationsInterface */
    private $contentOperations;

    public function __construct(
        Messages $messages,
        Crawler $crawler,
        ContentHashRepository $contentHashRepository,
        LocationHashRepository $locationHashRepository,
        ContentOperationsInterface $contentOperations
    ) {
        $this->messages = $messages;
        $this->crawler = $crawler;
        $this->contentHashRepository = $contentHashRepository;
        $this->locationHashRepository = $locationHashRepository;
        $this->contentOperations = $contentOperations;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->addArgument(
                self::ARGUMENT_LOCATION_ID,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_location_id')
            )
            ->setDescription($this->messages->get('command_set_root_hashes_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument(self::ARGUMENT_IDENTIFIER);
        try {
            $this->crawler->setHandler($identifier);
        } catch (InvalidArgumentException $e) {
            $io->error($e->getMessage());

            return 1;
        }

        try {
            $locationId = (int) $input->getArgument(self::ARGUMENT_LOCATION_ID);
            $location = $this->contentOperations->loadLocation($locationId);
        } catch (Exception $e) {
            $io->error($e->getMessage());

            return 1;
        }

        $content = $this->contentOperations->loadContent($location->getContentId());

        $this->contentHashRepository->removeByContentIds([$content->getId()]);
        $this->locationHashRepository->removeByLocationIds([$location->getId()]);

        try {
            $this->createHashes($location, $content);
        } catch (Exception $e) {
            $io->error($e->getMessage());

            return 1;
        }

        $io->success($this->messages->get('command_set_root_hashes_success', [$content->getName(), $content->getId(), $location->getId()]));

        return 0;
    }

    private function createHashes(Location $location, Content $content): void
    {
        $identifier = $this->crawler->getImportIdentifier();
        $url = $this->crawler->getHomePageUrl();

        $locationHash = new LocationHash($identifier, $url);
        $locationHash->setLocationId($location->getId());
        $locationHash->setHashAttributes(['url' => $url]);
        $this->locationHashRepository->store($locationHash);

        $contentHash = new ContentHash($identifier, $url);
        $contentHash->setHandler('');
        $contentHash->setSelector('');
        $contentHash->setContentId($content->getId());
        $contentHash->setContentVersion($content->getVersion());
        $contentHash->setTypeId($content->getContentType()->getId());
        $contentHash->setIsRoot(true);
        $contentHash->setHashAttributes([]);
        $this->contentHashRepository->store($contentHash);
    }
}
