<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\ContentImport\Exception\InvalidContentField;
use ContextualCode\Crawler\Entity\Page;

class MetaAttribute extends Html
{
    public const PARAM_META_ATTRIBUTE = 'attribute';

    public function getServiceIdentifier(): string
    {
        return 'meta-attribute';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $attribute = $this->getRequiredParameter($params, self::PARAM_META_ATTRIBUTE);

        try {
            $element = $this->selectElement($page, '//meta[@name="' . $attribute . '"]');
            $attribute = trim($element->getAttribute('content'));
        } catch(InvalidContentField $e) {
            $attribute = null;
        }

        $value = new ContentFieldValue($fieldName, $attribute);
        $this->checkValueIsRequired($params, $value);

        return $value;
    }
}
