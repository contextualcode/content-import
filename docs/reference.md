## Content Import Handler

All the Content Import Handlers need to extend [`ContextualCode\ContentImport\ContentHandler\ContentHandler`](../src/ContentHandler/ContentHandler.php) abstract class. It requires to implement some abstract functions and allows overriding default functions.

The list of abstract functions which require to be implemented in any Content Import Handler:

|Function|Description|
|---|---|
|`getImportIdentifier`|Returns the import identifier. It should match the appropriate crawler handler identifier.|
|`doSupport`|Checks if the specified `Page` entity is supported by the current content import handler. Should return boolean value.|
|`getContentType`|Should return a Content Type instance.|
|`getParentLocation`|Should return a parent Location instance for a specified `Page` entity.|
|`getFieldTransformers`|Returns the array which defines how the specified `Page` is converted to the content fields.|

The optional functions with their default implementation:

|Function|Description|
|---|---|
|`getPriority`|Returns the priority for the current content import handler. Each page is handled only by one Content Import Handler with the lowest priority number. The default value is `0`.|
|`getHashAttributes`|Returns the array which defines how the content field attributes are going to be hashed. By default `value` field hash transformer is used for all content fields.|

## Content Field Transformer

All the Content Field Transformers need to extend [`ContextualCode\ContentImport\FieldTransformer\Base`](../src/FieldTransformer/Base.php) abstract class, and have to implement following functions:

|Function|Description|
|---|---|
|`getServiceIdentifier`|Returns unique Content Field Transformer identifier. Example value "text".|
|`getFieldValue`|Receives the `Page` entity, string field name and array of parameters specified in the Content Import Handler. Returns an instance of [`ContextualCode\ContentImport\ContentHandler\ContentFieldValue`](../src/ContentHandler/ContentFieldValue.php) class.|
|`cleanup`|This function receives the value of the current field. And it is called after the content is published. Useful usage: cleanup downloaded local binaries for files/images. This function is empty by default.|

This package provides a list of built-in Content Field Transformers. Which might be reused/extended in CMS/CMR/DXP specific implementation:

|Identifier|Description|Parameters|
|---|---|---|
|`file`|Locally downloads the file provided as `url` parameter. And returns its basic metadata.|- `url`: remote file URL|
|`file-name`|Retrieves file name for the current page from its referer page.|- `required`: the return value can not be empty if this parameter is specified|
|`image`|Locally downloads the image provided as `url` parameter. And returns its basic metadata.|- `url`: remote image URL|
|`image-name`|Retrieves image name for the current page from its referer page.|- `required`: the return value can not be empty if this parameter is specified|
|`static-value`|Returns static value specified as `value` parameter.|- `value`: static value to return|
|`html`|Extracts HTML content for the current page DOM element specified in `selector` parameter.|- `selector`: XPath selector <br /> - `required`: the return value can not be empty if this parameter is specified|
|`text`|Extracts text content for the current page DOM element specified in `selector` parameter.|- `selector`: XPath selector <br /> - `required`: the return value can not be empty if this parameter is specified|
|`text-line`|Extracts single line text content for the current page DOM element specified in `selector` parameter.|- `selector`: XPath selector <br /> - `required`: the return value can not be empty if this parameter is specified|

## Field Hash Transformer

All the Field Hash Transformers need to extend [`ContextualCode\ContentImport\FieldHashTransformer\Base`](../src/FieldHashTransformer/Base.php) abstract class, and have to implement following functions:

|Function|Description|
|---|---|
|`getServiceIdentifier`|Returns unique Field Hash Transformer identifier. Example value "value".|
|`getAttributes`|Receives the `Page` entity, field value and array of parameters specified in the Content Import Handler. Returns the list of scalar attributes, which will be used to calculate the content hash.|

This package provides a list of built-in Field Hash Transformers. Which might be reused/extended in CMS/CMR/DXP specific implementation:

|Identifier|Description|
|---|---|
|`value`|Converts current field value to a string and returns it as a single attribute. This is default Field Hash Transformer for all the fields.|
|`file`|Returns two attributes: original file name and original file size.|
|`image`|Returns two attributes: original image name and original image size.|

## Content Operations

Content Operations is a service that implements [`ContextualCode\ContentImport\ContentHandler\ContentOperationsInterface`](../src/ContentHandler/ContentOperationsInterface.php). This package provides a dummy example implementation: [`ContextualCode\ContentImport\Service\Integration\ContentOperations`](../src/Service/Integration/ContentOperations.php).

## Commands

Most of the commands in this package expect to receive the content import identifier as the argument. It should match the appropriate crawler handler identifier.

### `content-import:run`

This command imports the content from the previously crawled data.

It has `--update` additional option. Changed content will be updated if this option is specified and if there were no manual edits made.

Example usage:
```bash
$ php bin/console content-import:run help-flcourts

 To get live logs, please run the following command in a new terminal:  
 tail -f /XXX/var/log/contextualcode-content-import.log

Importing the content ...
=========================

100/100 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% 58 secs/58 secs

Importing custom URL aliases (redirects) ...
============================================

4/4 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100% < 1 sec/< 1 sec

 Content import completed:
  * 98 new content items were created
  * 0 existing content items were updated
  * 0 new locations were assigned
  * 3 url redirects were created
```

### `content-import:set-root-hash`

This command creates a content and location hash for an existing root location. This command needs to be used only when the root location of the imported site already exists in the content structure, and no new location is required to be published for it.

It expects an additional `location_id` argument.

Example usage:
```bash
$ php bin/console content-import:set-root-hash help-flcourts 2850

 [OK] Import hashes were set for "Florida Court Help" (contentId: 2832, locationId: 2850)
```

### `content-import:info`

Do not expect to receive any arguments, and provides information about:

- Crawler handlers
- Content import handlers
- Content field transformers
- Field hash transformers
- Content operations handler

Example usage:
```bash
$ php bin/console content-import:info

Crawler handlers (2):
=====================

 ---------------- -----------------------------------------
  Identifier       Class
 ---------------- -----------------------------------------
  help-flcourts    App\ContentImport\HelpFlcourts\Crawler
  school-justice   App\ContentImport\SchoolJustice\Crawler
 ---------------- -----------------------------------------

Content import handlers (8):
============================

 ---------- ----------------------------------- ---------------------------------------------------------
  Priority   Identifier                          Class
 ---------- ----------------------------------- ---------------------------------------------------------
  0          school-justice-microsite_homepage   App\ContentImport\SchoolJustice\ContentHandler\Homepage
  0          help-flcourts-microsite_homepage    App\ContentImport\HelpFlcourts\ContentHandler\Homepage
  10         help-flcourts-file                  App\ContentImport\HelpFlcourts\ContentHandler\File
  10         help-flcourts-image                 App\ContentImport\HelpFlcourts\ContentHandler\Image
  10         school-justice-image                App\ContentImport\SchoolJustice\ContentHandler\Image
  11         school-justice-file                 App\ContentImport\SchoolJustice\ContentHandler\File
  100        help-flcourts-folder                App\ContentImport\HelpFlcourts\ContentHandler\Folder
  100        school-justice-folder               App\ContentImport\SchoolJustice\ContentHandler\Folder
 ---------- ----------------------------------- ---------------------------------------------------------

Field transformers (11):
========================

 -------------- ---------------------------------------------------------------------------
  Identifier     Class
 -------------- ---------------------------------------------------------------------------
  file           ContextualCode\ContentImport\FieldTransformer\File
  file-name      ContextualCode\ContentImport\FieldTransformer\FileName
  html           ContextualCode\ContentImport\FieldTransformer\Html
  image          ContextualCode\ContentImport\FieldTransformer\Image
  image-name     ContextualCode\ContentImport\FieldTransformer\ImageName
  static-value   ContextualCode\ContentImport\FieldTransformer\StaticValue
  text           ContextualCode\ContentImport\FieldTransformer\Text
  text-line      ContextualCode\ContentImport\FieldTransformer\TextLine
  ez-file        ContextualCode\EzPlatformContentImport\FieldTransformer\File
  ez-image       ContextualCode\EzPlatformContentImport\FieldTransformer\Image
  ez-richtext    ContextualCode\EzPlatformContentImport\FieldTransformer\RichText\RichText
 -------------- ---------------------------------------------------------------------------

Field hash transformers (5):
============================

 ------------ -------------------------------------------------------------------
  Identifier   Class
 ------------ -------------------------------------------------------------------
  file         ContextualCode\ContentImport\FieldHashTransformer\File
  image        ContextualCode\ContentImport\FieldHashTransformer\Image
  value        ContextualCode\ContentImport\FieldHashTransformer\Value
  ez-file      ContextualCode\EzPlatformContentImport\FieldHashTransformer\File
  ez-image     ContextualCode\EzPlatformContentImport\FieldHashTransformer\Image
 ------------ -------------------------------------------------------------------

Content operations handler:
===========================

 ------------------------------------------------------------------
  Class
 ------------------------------------------------------------------
  ContextualCode\EzPlatformContentImport\Service\ContentOperations
 ------------------------------------------------------------------
```
