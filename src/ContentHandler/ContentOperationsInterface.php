<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

use ContextualCode\ContentImport\ContentHandler\ContentInterface as Content;
use ContextualCode\ContentImport\ContentHandler\LocationInterface as Location;

interface ContentOperationsInterface
{
    public function loadContent(int $id): Content;

    public function loadLocation(int $id): Location;

    public function createContent(
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields
    ): Content;

    public function updateContent(
        Content $content,
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields
    ): Content;

    public function createNewLocation(
        Content $hash,
        PageContentItem $contentItem,
        ContentHandler $handler
    ): ?Location;

    public function createRedirect(
        int $locationId,
        PageContentItem $contentItem,
        ContentHandler $handler
    ): bool;

    public function postImport(Content $content): void;

    public function preTransform(PageContentItem $contentItem, ContentHandler $handler): void;

    public function postTransform(PageContentItem $contentItem, ContentHandler $handler): void;
}
