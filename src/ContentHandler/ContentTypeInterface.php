<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\ContentHandler;

interface ContentTypeInterface
{
    public function getId(): int;

    public function getIdentifier(): string;
}
