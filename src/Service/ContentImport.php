<?php

namespace ContextualCode\ContentImport\Service;

use ContextualCode\ContentImport\ContentHandler\ContentHandler;
use ContextualCode\ContentImport\ContentHandler\ContentInterface as Content;
use ContextualCode\ContentImport\ContentHandler\ContentOperationsInterface;
use ContextualCode\ContentImport\ContentHandler\LocationInterface as Location;
use ContextualCode\ContentImport\ContentHandler\PageContentItem;
use ContextualCode\ContentImport\Entity\Hash\Content as ContentHash;
use ContextualCode\ContentImport\Entity\Hash\Location as LocationHash;
use ContextualCode\ContentImport\Exception\Base as ContentImportException;
use ContextualCode\ContentImport\Exception\ContentHandlerNotFound;
use ContextualCode\ContentImport\Exception\ContentHashVersionMismatch;
use ContextualCode\ContentImport\Exception\ContentNotFound;
use ContextualCode\ContentImport\Exception\InvalidContentField;
use ContextualCode\ContentImport\FieldHashTransformer\Base as FieldHashTransformer;
use ContextualCode\ContentImport\FieldTransformer\Base as FieldTransformer;
use ContextualCode\ContentImport\Repository\Hash\ContentRepository as ContentHashRepository;
use ContextualCode\ContentImport\Repository\Hash\LocationRepository as LocationHashRepository;
use ContextualCode\Crawler\Entity\Page;
use ContextualCode\Crawler\Repository\PageRepository;
use Monolog\Handler\FingersCrossedHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class ContentImport
{
    public const PARAM_CONTENT_ITEM_SELECTOR = 'DOMSelector';
    public const PARAM_MERGE = 'merge';

    /** @var LoggerInterface */
    private $logger;

    /** @var ContentHandler[] */
    private $contentHandlers;

    /** @var FieldTransformer[] */
    private $fieldTransformers;

    /** @var FieldHashTransformer[] */
    private $fieldHashTransformers;

    /** @var PageRepository */
    private $pageRepository;

    /** @var ContentHashRepository */
    private $contentHashRepository;

    /** @var LocationHashRepository */
    private $locationHashRepository;

    /** @var ContentOperationsInterface */
    private $contentOperations;

    /** @var Messages */
    private $messages;

    /** @var ProgressBar */
    private $progressBar;

    /** @var array */
    private $counter = [
        'created' => 0,
        'updated' => 0,
        'location_added' => 0,
        'redirect_added' => 0,
    ];

    public function __construct(
        LoggerInterface $contentImportLogger,
        iterable $contentHandlers,
        iterable $fieldTransformers,
        iterable $fieldHashTransformers,
        PageRepository $pageRepository,
        ContentHashRepository $contentHashRepository,
        LocationHashRepository $locationHashRepository,
        ContentOperationsInterface $contentOperations,
        Messages $messages
    ) {
        $this->logger = $contentImportLogger;

        $this->contentHandlers = $this->getHandlers($contentHandlers, ContentHandler::class);
        $this->fieldTransformers = $this->getHandlers($fieldTransformers, FieldTransformer::class);
        $this->fieldHashTransformers = $this->getHandlers($fieldHashTransformers, FieldHashTransformer::class);

        $this->pageRepository = $pageRepository;
        $this->contentHashRepository = $contentHashRepository;
        $this->locationHashRepository = $locationHashRepository;
        $this->contentOperations = $contentOperations;
        $this->messages = $messages;
    }

    protected function getHandlers(iterable $handlers, string $type): array
    {
        $return = [];

        foreach ($handlers as $handler) {
            if (!$handler instanceof $type) {
                continue;
            }

            $return[$handler->getServiceIdentifier()] = $handler;
        }

        if ($type === ContentHandler::class) {
            uasort($return, static function ($a, $b) {
                return $a->getPriority() <=> $b->getPriority();
            });
        }

        return $return;
    }

    public function getHandler(string $identifier): ?ContentHandler
    {
        return $this->contentHandlers[$identifier] ?? null;
    }

    public function setProgressBar(ProgressBar $progressBar): void
    {
        $this->progressBar = $progressBar;
    }

    public function getLogFilepath(): string
    {
        $handler = $this->logger->getHandlers()[0];
        if ($handler instanceof FingersCrossedHandler) {
            $handler = $handler->getHandler();
        }

        return $handler->getUrl();
    }

    public function importPages(
        string $identifier,
        bool $update,
        bool $updateLocations,
        int $offset = null,
        int $limit = null
    ): void {
        $pages = $this->getPages($identifier, false, $offset, $limit);

        foreach ($pages as $page) {
            try {
                $this->importPage($page, $update, $updateLocations);
            } catch (ContentImportException $e) {
                $this->logger->critical($e->getMessage(), $e->getContext());
            }

            $this->renderProgressBar();
        }
    }

    public function importRedirects(string $identifier): void
    {
        $pages = $this->getPages($identifier, true);

        foreach ($pages as $page) {
            try {
                $this->importRedirect($page);
            } catch (ContentImportException $e) {
                $this->logger->error($e->getMessage());
            }

            $this->renderProgressBar();
        }
    }

    public function getStatistics(): array
    {
        return $this->counter;
    }

    public function getPages(
        string $identifier,
        bool $isRedirect = false,
        int $offset = null,
        int $limit = null
    ): array {
        $criteria = [
            'identifier' => $identifier,
            'isRedirect' => $isRedirect,
            'isValid' => true,
        ];
        $pages = $this->pageRepository->findBy($criteria, null, $limit, $offset);

        $this->renderProgressBar(count($pages));

        return $pages;
    }

    public function getContentImportHandlers(Page $page): array
    {
        $handlers = [];

        foreach ($this->contentHandlers as $handler) {
            if ($handler->doSupport($page) && $handler->getImportIdentifier() === $page->getIdentifier()) {
                $handlers[] = $handler;

                if($handler->isFinal()) {
                    break;
                }
            }
        }

        if (count($handlers) > 0) {
            return $handlers;
        }

        throw new ContentHandlerNotFound($this->messages->get('error_content_handler_not_found', [$page->getUrl()]));
    }

    public function getDefaultContentImportHandler(Page $page): ?ContentHandler
    {
        $handlers = $this->getContentImportHandlers($page);

        foreach ($handlers as $handler) {
            if ($handler->isActive()) {
                return $handler;
            }
        }

        return null;
    }

    public function getContentFields(PageContentItem $contentItem, ContentHandler $handler): array
    {
        $page = $contentItem->getPage();
        $fields = [];

        $availableFieldTransformers = array_keys($this->fieldTransformers);
        foreach ($handler->getFieldTransformers($page, $contentItem) as $field => $definition) {
            try {
                $transformer = $this->getContentFieldTransformer($definition);

                $params = $definition['params'] ?? [];
                $params[self::PARAM_CONTENT_ITEM_SELECTOR] = $contentItem->getDOMSelector();

                $merge = $params[self::PARAM_MERGE] ?? false;
                
                $fieldValue = $transformer->getFieldValue($page, $field, $params);
                if (null !== $fieldValue->getValue()) {
                    if ($merge) {
                        $fields[$field] = [
                            'merge' => true,
                            'value' => $fieldValue,
                        ];
                    }
                    else {
                        $fields[$field] = $fieldValue;
                    }
                }
            } catch (InvalidContentField $e) {
                $e->setContextKey('url', $page->getUrl());
                $e->setContextKey('field', $field);
                $e->setContextKey('handler', $handler->getServiceIdentifier());
                $e->setContextKey('field_transformer', $definition);
                $e->setContextKey('available_field_transformers', $availableFieldTransformers);

                throw $e;
            }
        }

        return $fields;
    }

    public function getHashAttributes(
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields
    ): array {
        $attributes = [];

        $availableHashTransformers = array_keys($this->fieldHashTransformers);
        foreach ($handler->getHashAttributes($contentItem->getPage(), $contentItem) as $field => $definition) {
            if (!isset($fields[$field])) {
                continue;
            }

            try {
                $transformer = $this->getContentHashTransformer($definition);
                $fieldValue = $fields[$field];
                if (is_array($fieldValue) && isset($fieldValue['merge']) && isset($fieldValue['value'])) {
                    $fieldValue = $fieldValue['value'];
                }
                $fieldHashAttributes = $transformer->getAttributes($contentItem->getPage(), $fieldValue, $definition['params'] ?? []);
                foreach ($fieldHashAttributes as $id => $value) {
                    $attributes[$id] = $value;
                }
            } catch (InvalidContentField $e) {
                $e->setContextKey('field', $field);
                $e->setContextKey('handler', $handler->getServiceIdentifier());
                $e->setContextKey('field_hash_transformer', $definition);
                $e->setContextKey('available_field_hash_transformers', $availableHashTransformers);

                throw $e;
            }
        }

        return $attributes;
    }

    private function importPage(Page $page, bool $update, bool $updateLocations): void
    {
        $handlers = $this->getContentImportHandlers($page);

        foreach ($handlers as $handler) {
            if (!$handler->isActive()) {
                continue;
            }

            $contentItems = $handler->extractContentItems($page);
            foreach ($contentItems as $contentItem) {
                $operation = 'create';
                $hash = $this->contentHashRepository->findOneBy([
                    'identifier' => $page->getIdentifier(),
                    'url' => $contentItem->getHashUrl(),
                    //'handler' => $handler->getServiceIdentifier(),
                ]);
                if ($hash instanceof ContentHash) {
                    if (!$update) {
                        $message = $this->messages->get('skip_content_update', [$hash->getContentId(), $page->getUrl()]);
                        $this->logger->debug($message);

                        continue;
                    }

                    $operation = 'update';
                }

                $this->contentOperations->preTransform($contentItem, $handler);
                $fields = $this->getContentFields($contentItem, $handler);
                $this->contentOperations->postTransform($contentItem, $handler);

                if ($operation === 'create') {
                    $content = $this->createContent($contentItem, $handler, $fields);
                } elseif ($operation === 'update') {
                    $content = $this->updateContent($contentItem, $handler, $fields, $hash);

                    if ($updateLocations && !$hash->getIsRoot()) {
                        $this->addLocation($content, $contentItem, $handler);
                    }
                }

                $this->cleanupContentFields($handler->getFieldTransformers($page, $contentItem), $fields);
            }
        }
    }

    private function createContent(
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields
    ): Content {
        $attributes = $this->getHashAttributes($contentItem, $handler, $fields);
        $hash = new ContentHash($contentItem->getPage()->getIdentifier(), $contentItem->getHashUrl());
        $hash->setHandler($handler->getServiceIdentifier());
        $hash->setSelector($contentItem->getDOMSelector());
        $hash->setHashAttributes($attributes, true);

        if ($handler->uniqueContent()) {
            $existingHash = $this->contentHashRepository->findOneBy(['hash' => $hash->getHash()]);
            if ($existingHash instanceof ContentHash) {
                $message = $this->messages->get('content_already_exist',
                    [$existingHash->getContentId(), $contentItem->getHashUrl()]);

                $this->logger->debug($message);

                // We are updating existing content only when the content item is found
                try {
                    $content = $this->contentOperations->loadContent($existingHash->getContentId());

                    return $this->updateContent($contentItem, $handler, $fields, $existingHash);
                } catch (ContentNotFound $e) {
                }
            }
        }

        $content = $this->contentOperations->createContent($contentItem, $handler, $fields);
        ++$this->counter['created'];

        $hash->setContentId($content->getId());
        $hash->setContentVersion($content->getVersion());
        $hash->setTypeId($content->getContentType()->getId());
        $this->contentHashRepository->store($hash);

        $this->createNewLocationHash($contentItem, $content->getMainLocation()->getId());

        $this->contentOperations->postImport($content);

        return $content;
    }

    private function updateContent(
        PageContentItem $contentItem,
        ContentHandler $handler,
        array $fields,
        ContentHash $hash
    ): Content {
        try {
            $content = $this->contentOperations->loadContent($hash->getContentId());
        } catch (ContentNotFound $e) {
            $message = $this->messages->get('content_was_removed', [$hash->getContentId(), $contentItem->getHashUrl()]);
            $this->logger->error($message);

            $identifier = $contentItem->getPage()->getIdentifier();
            $this->contentHashRepository->removeByPage($identifier, $contentItem->getHashUrl());
            $this->locationHashRepository->removeByPage($identifier, $contentItem->getHashUrl());

            return $this->createContent($contentItem, $handler, $fields);
        }

        $hashAttributes = $this->getHashAttributes($contentItem, $handler, $fields);
        if ($hash->isTheSame($hashAttributes)) {
            return $content;
        }

        if ($content->getVersion() !== $hash->getContentVersion()) {
            $context = [
                'content_id' => $content->getId(),
                'content_current_version' => $content->getVersion(),
                'content_hash_version' => $hash->getContentVersion(),
            ];
            $message = $this->messages->get('content_hash_version_mismatch', $context);

            throw new ContentHashVersionMismatch($message, $context);
        }

        $content = $this->contentOperations->updateContent($content, $contentItem, $handler, $fields);
        ++$this->counter['updated'];

        $hash->setContentVersion($content->getVersion());
        $hash->setHashAttributes($hashAttributes, true);
        $this->contentHashRepository->store($hash);

        $this->contentOperations->postImport($content);

        return $content;
    }

    private function addLocation(
        Content $content,
        PageContentItem $contentItem,
        ContentHandler $handler
    ): ?Location {
        $location = $this->contentOperations->createNewLocation($content, $contentItem, $handler);

        if (null !== $location) {
            ++$this->counter['location_added'];

            $this->createNewLocationHash($contentItem, $location->getId());
        }

        return $location;
    }

    private function importRedirect(Page $page): void
    {
        if ($this->locationHashRepository->findByPage($page)) {
            return;
        }

        $destinationLocationHash = $this->getDestinationLocationHash($page);
        if (null === $destinationLocationHash) {
            $message = $this->messages->get('error_no_final_destination_page', [$page->getUrl()]);
            $this->logger->error($message);

            return;
        }

        $handler = $this->getDefaultContentImportHandler($page);
        if ($handler === null) {
            return;
        }

        $contentPage = new PageContentItem($page, '', $page->getUrl());
        if ($this->contentOperations->createRedirect($destinationLocationHash->getLocationId(), $contentPage, $handler)) {
            ++$this->counter['redirect_added'];

            $this->createNewLocationHash($contentPage, $destinationLocationHash->getLocationId(), true);
        }
    }

    private function renderProgressBar(?int $max = null): void
    {
        if (!$this->progressBar instanceof ProgressBar) {
            return;
        }

        if ($max > 0) {
            $this->progressBar->setMaxSteps(max(1, $max));
        }

        $this->progressBar->advance();
    }

    private function getContentFieldTransformer(array $transformer): FieldTransformer
    {
        if (!isset($transformer['id'])) {
            throw new InvalidContentField($this->messages->get('error_field_transformer_no_id'));
        }

        if (!isset($this->fieldTransformers[$transformer['id']])) {
            throw new InvalidContentField($this->messages->get('error_field_transformer_not_found', [$transformer['id']]));
        }

        return $this->fieldTransformers[$transformer['id']];
    }

    private function getContentHashTransformer(array $transformer): FieldHashTransformer
    {
        if (!isset($transformer['id'])) {
            throw new InvalidContentField($this->messages->get('error_field_hash_transformer_no_id'));
        }

        if (!isset($this->fieldHashTransformers[$transformer['id']])) {
            throw new InvalidContentField($this->messages->get('error_field_hash_transformer_not_found', [$transformer['id']]));
        }

        return $this->fieldHashTransformers[$transformer['id']];
    }

    private function createNewLocationHash(
        PageContentItem $contentItem,
        int $locationId,
        bool $isRedirect = false
    ): LocationHash {
        $hash = new LocationHash($contentItem->getPage()->getIdentifier(), $contentItem->getHashUrl());
        $hash->setLocationId($locationId);
        $hash->setHashAttributes(['url' => $contentItem->getHashUrl()], true);
        $hash->setRedirect($isRedirect);

        $this->locationHashRepository->store($hash);

        return $hash;
    }

    private function getDestinationLocationHash(Page $startPage): ?LocationHash
    {
        $page = $startPage;
        $identifier = $page->getIdentifier();
        $handledUrls = [$page->getUrl()];

        while (true) {
            $page = $this->pageRepository->findOneBy([
                'identifier' => $identifier,
                'url' => $page->getRedirectUrl(),
            ]);

            if (null === $page) {
                $startPage->setErrorMessage('Unable to find destination page');
                $startPage->setValid(false);
                $this->pageRepository->store($startPage);

                return null;
            }

            if (in_array($page->getUrl(), $handledUrls, true)) {
                $startPage->setErrorMessage('Infinite redirect loop');
                $startPage->setValid(false);
                $this->pageRepository->store($startPage);

                return null;
            }
            $handledUrls[] = $page->getUrl();

            if (!$page->isRedirect()) {
                break;
            }
        }

        return $this->locationHashRepository->findOneBy([
            'identifier' => $identifier,
            'url' => $page->getUrl(),
        ]);
    }

    private function cleanupContentFields(array $transformers, array $fields): void
    {
        foreach ($fields as $field => $value) {
            $transformer = $this->getContentFieldTransformer($transformers[$field]);
            if (is_array($value) && isset($value['merge']) && isset($value['value'])) {
                $transformer->cleanup($value['value']);
            }
            else {
                $transformer->cleanup($value);
            }
        }
    }
}
