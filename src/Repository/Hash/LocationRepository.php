<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Repository\Hash;

use ContextualCode\ContentImport\Entity\Hash\Location as Entity;
use Doctrine\Persistence\ManagerRegistry;

class LocationRepository extends Base
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }

    public function store(Entity $hash): void
    {
        $this->storeEntity($hash);
    }

    public function removeByPage(string $identifier, string $page): void
    {
        $this->removeEntityByPage($identifier, $page, Entity::class);
    }

    public function removeByLocationIds(array $locationIds): void
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->delete(Entity::class, 'h')
            ->andWhere('h.locationId IN (:locationIds)')
            ->setParameter('locationIds', $locationIds);

        $qb->getQuery()->execute();
    }

    public function updateLocationId(int $oldLocationId, int $newLocationId): void
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->update(Entity::class, 'h')
            ->set('h.locationId', $newLocationId)
            ->where('h.locationId = :oldLocationId')
            ->setParameter('oldLocationId', $oldLocationId);

        $qb->getQuery()->execute();
    }
}
