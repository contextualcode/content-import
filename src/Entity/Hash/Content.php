<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\Entity\Hash;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ContextualCode\ContentImport\Repository\Hash\ContentRepository")
 * @ORM\Table(name="cc_content_import_content_hashes",indexes={@ORM\Index(name="content_id_idx", columns={"content_id"})})
 */
class Content extends Base
{
    /**
     * @ORM\Column(type="string", length=32)
     * @ORM\Id
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", length=736)
     * @ORM\Id
     */
    protected $url;

    /** @ORM\Column(type="string", length=32) */
    protected $hash;

    /** @ORM\Column(type="json", nullable=true) */
    protected $hashAttributes;

    /** @ORM\Column(type="json", nullable=true) */
    protected $extraData;

    /**
     * @ORM\Column(type="string", length=256)
     * @ORM\Id
     */
    private $handler;

    /**
     * @ORM\Column(type="string", length=256)
     * @ORM\Id
     */
    private $selector;

    /** @ORM\Column(type="integer", length=11) */
    private $contentId;

    /** @ORM\Column(type="integer", length=11) */
    private $contentVersion;

    /** @ORM\Column(type="integer", length=11) */
    private $typeId;

    /** @ORM\Column(type="boolean", nullable=true, options={"default": false}) */
    private $isRoot = false;

    public function __construct(string $identifier, string $url, array $hashAttributes = [])
    {
        parent::__construct($identifier, $url);

        if (count($hashAttributes) > 0) {
            $this->setHashAttributes($hashAttributes);
        }
    }

    public function getHandler(): string
    {
        return $this->handler;
    }

    public function getSelector(): string
    {
        return $this->selector;
    }

    public function getContentId(): ?int
    {
        return $this->contentId;
    }

    public function getContentVersion(): ?int
    {
        return $this->contentVersion;
    }

    public function getTypeId(): ?int
    {
        return $this->typeId;
    }

    public function getIsRoot(): bool
    {
        return $this->isRoot;
    }

    public function getExtraData(): array
    {
        return (array) $this->extraData;
    }

    public function setContentId(int $contentId): void
    {
        $this->contentId = $contentId;
    }

    public function setHandler(string $handler): void
    {
        $this->handler = $handler;
    }

    public function setSelector(string $selector): void
    {
        $this->selector = $selector;
    }

    public function setContentVersion(int $contentVersion): void
    {
        $this->contentVersion = $contentVersion;
    }

    public function setTypeId(int $typeId): void
    {
        $this->typeId = $typeId;
    }

    public function setIsRoot(bool $isRoot): void
    {
        $this->isRoot = $isRoot;
    }

    public function setExtraData(array $data): void
    {
        $this->extraData = $data;
    }
}
