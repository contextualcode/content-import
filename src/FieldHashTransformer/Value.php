<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldHashTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

class Value extends Base
{
    public const SERVICE_IDENTIFIER = 'value';

    public function getServiceIdentifier(): string
    {
        return self::SERVICE_IDENTIFIER;
    }

    public function getAttributes(
        Page $page,
        ContentFieldValueInterface $fieldValue,
        array $params = []
    ): array {
        $value = $fieldValue->getValue();

        return [$fieldValue->getName() => is_array($value) ? serialize($value) : (string) $value];
    }
}
