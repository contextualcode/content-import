<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValue;
use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

class ImageName extends FileName
{
    public function getServiceIdentifier(): string
    {
        return 'image-name';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $name = $this->getFileName($page, ['title', 'alt'], false);

        $value = new ContentFieldValue($fieldName, $name);
        $this->checkValueIsRequired($params, $value);

        return $value;
    }
}
