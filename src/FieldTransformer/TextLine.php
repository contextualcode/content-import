<?php

declare(strict_types=1);

namespace ContextualCode\ContentImport\FieldTransformer;

use ContextualCode\ContentImport\ContentHandler\ContentFieldValueInterface;
use ContextualCode\Crawler\Entity\Page;

class TextLine extends Text
{
    public function getServiceIdentifier(): string
    {
        return 'text-line';
    }

    public function getFieldValue(
        Page $page,
        string $fieldName,
        array $params = []
    ): ContentFieldValueInterface {
        $value = parent::getFieldValue($page, $fieldName, $params);

        $textLine = trim(preg_replace('/\s+/', ' ', (string) $value->getValue()));
        $value->setValue($textLine);

        return $value;
    }
}
